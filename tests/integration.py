
import os
import sys
import json
from webtest import TestApp
from pyvows import Vows, expect
from bs4 import BeautifulSoup
sys.path.append("../server")
#os.chdir("./server")
import manuela

test_server = TestApp(manuela.app)


@Vows.batch
class ApiTest(Vows.Context):
    class Brand(Vows.Context):
        def topic(self):
            return test_server.get("/api/brands/16")

        def http_status(self, topic):
            expect(topic.content_type).to_equal('application/json')
            expect(topic.status).to_equal("200 OK")

        def have_valid_keys(self, topic):
            json_body = json.loads(topic.body)
            expect(json_body).to_include("name")
            expect(json_body).to_include("id")
            expect(json_body.get("name")).to_be_instance_of(unicode)
            expect(json_body.get("id")).to_be_instance_of(int)

    class Brands(Vows.Context):
        def topic(self):
            return test_server.get("/api/brands")

        def http_status(self, topic):
            expect(topic.content_type).to_equal('application/json')
            expect(topic.status).to_equal("200 OK")

        def have_valid_keys(self, topic):
            json_body = json.loads(topic.body)
            expect(json_body).to_include("alphabet")
            expect(json_body.get("alphabet")).to_be_instance_of(list)

    class BrandsLetter(Vows.Context):
        def topic(self):
            return test_server.get("/api/brands/a")

        def http_status(self, topic):
            expect(topic.content_type).to_equal('application/json')
            expect(topic.status).to_equal("200 OK")

        def have_valid_keys(self, topic):
            json_body = json.loads(topic.body)
            expect(json_body).to_include("brands")
            expect(json_body.get("brands")).to_be_instance_of(list)

    class BrandCategories(Vows.Context):
        def topic(self):
            return test_server.get("/api/brands/16/categories")

        def http_status(self, topic):
            expect(topic.content_type).to_equal('application/json')
            expect(topic.status).to_equal("200 OK")

        def have_valid_keys(self, topic):
            json_body = json.loads(topic.body)
            expect(json_body).to_include("brand")
            expect(json_body).to_include("categories")
            expect(json_body.get("brand")).to_be_instance_of(dict)
            expect(json_body.get("categories")).to_be_instance_of(list)

    class BrandCategoryProducts(Vows.Context):
        def topic(self):
            return test_server.get("/api/brands/16/category/36")

        def http_status(self, topic):
            expect(topic.content_type).to_equal('application/json')
            expect(topic.status).to_equal("200 OK")

        def have_valid_keys(self, topic):
            json_body = json.loads(topic.body)
            expect(json_body).to_include("brand")
            expect(json_body).to_include("category")
            expect(json_body).to_include("products")
            expect(json_body.get("brand")).to_be_instance_of(dict)
            expect(json_body.get("category")).to_be_instance_of(dict)
            expect(json_body.get("products")).to_be_instance_of(list)

    class Category(Vows.Context):
        def topic(self):
            return test_server.get("/api/categories/36")

        def http_status(self, topic):
            expect(topic.content_type).to_equal('application/json')
            expect(topic.status).to_equal("200 OK")

        def have_valid_keys(self, topic):
            json_body = json.loads(topic.body)
            expect(json_body).to_include("title")
            expect(json_body).to_include("id")
            expect(json_body.get("title")).to_be_instance_of(unicode)
            expect(json_body.get("id")).to_be_instance_of(int)

    class Categories(Vows.Context):
        def topic(self):
            return test_server.get("/api/categories")

        def http_status(self, topic):
            expect(topic.content_type).to_equal('application/json')
            expect(topic.status).to_equal("200 OK")

        def have_valid_keys(self, topic):
            json_body = json.loads(topic.body)
            expect(json_body).to_include("categories")
            expect(json_body.get("categories")).to_be_instance_of(dict)

    class CategoryBrands(Vows.Context):
        def topic(self):
            return test_server.get("/api/categories/36/brands")

        def http_status(self, topic):
            expect(topic.content_type).to_equal('application/json')
            expect(topic.status).to_equal("200 OK")

        def have_valid_keys(self, topic):
            json_body = json.loads(topic.body)
            expect(json_body).to_include("brands")
            expect(json_body).to_include("category")
            expect(json_body.get("brands")).to_be_instance_of(list)
            expect(json_body.get("category")).to_be_instance_of(dict)

    class Product(Vows.Context):
        def topic(self):
            return test_server.get("/api/product/1")

        def http_status(self, topic):
            expect(topic.content_type).to_equal('application/json')
            expect(topic.status).to_equal("200 OK")

        def have_valid_keys(self, topic):
            json_body = json.loads(topic.body)
            expect(json_body).to_include("model")
            expect(json_body).to_include("filename")
            expect(json_body).to_include("id")

    class Search(Vows.Context):
        def topic(self):
            return test_server.get("/api/search?keyword=apple iphone&per_page=20&page=4")

        def http_status(self, topic):
            expect(topic.content_type).to_equal('application/json')
            expect(topic.status).to_equal("200 OK")

        def have_valid_keys(self, topic):
            json_body = json.loads(topic.body)
            expect(json_body).to_include("total_found")
            expect(json_body).to_include("total_pages")
            expect(json_body).to_include("products")
            expect(json_body).to_include("page")
            expect(json_body).to_include("keyword")
            expect(json_body.get("page")).to_equal(4)
            expect(json_body.get("keyword")).to_be_instance_of(unicode)
            expect(json_body.get("products")).to_be_instance_of(list)
            expect(json_body.get("total_pages")).to_be_instance_of(int)
            expect(json_body.get("total_found")).to_be_instance_of(int)
