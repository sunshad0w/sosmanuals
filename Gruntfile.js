"use strict";

module.exports = function(grunt) {
    
    // Project configuration.
    grunt.initConfig({
        'css-include-combine': {

            libs: {

                // include's on CSS will use this PATH as it's relative 
                // path. This also is applied to resources url's.
                relativeDir: 'www/css',

                // your main file (see example above)
                main: 'www/css/main.css',

                // the generated file
                out: 'www/css/main.min.css'

            }

        }
    });

    /* These plugins provide necessary tasks. */
    grunt.loadNpmTasks('grunt-css-include-combine');

    /* Tasks */
    grunt.registerTask("build",["css-include-combine"]);
};
