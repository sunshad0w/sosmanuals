#!/usr/bin/env python
# Manuals online Data & PDF Scrapper

import os
import sys
import json

import requests
import pymysql
import bs4


SCRAP_TARGET = "http://www.manualsonline.com/"
queries = dict(
    insert_brand="INSERT INTO brands(name) values(%s)",
    insert_category="INSERT INTO categories(title) VALUES(%s) ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), title=title",
    insert_product="INSERT INTO products(model, description, category, brand, filename) VALUES(%s, %s, %s, %s, %s)")


def get_brands(brands_url, verbose=False, cache_file="cache.txt"):
    """
        Retrieve all available brands.
        Important!: returned url is a relative
        Returns: Iterator
    """

    cached_brands = get_cached_brands(cache_file)
    print "Loaded from cache: {}".format(cached_brands)
    cache_file = open(cache_file, "a")

    brands_page = requests.get(brands_url)
    soup = bs4.BeautifulSoup(brands_page.text)

    brand_containers = soup.find("div", id="searchResultsContainer").find_all("ul")
    for i, brand_container in enumerate(brand_containers):
        for brand in brand_container.find_all("li"):
            name = brand.a.text.encode("utf-8")
            url = brand.a.get("href")
            if verbose:
                print "Processing brand: {}".format(name)

            if url in cached_brands:
                continue

            cache_file.write(url.encode("utf-8") + "\n")
            yield name, url

    cache_file.close()


def get_brand_categories(brand_url, recurse=False):
    """
        Retrieve all categories of a brand.
        Important!: returned url is a relative
        Returns: Iterator
    """
    scrap_url = brand_url if not recurse else "".join([brand_url, recurse])
    brand_categories = requests.get(scrap_url)
    soup = bs4.BeautifulSoup(brand_categories.text)

    try:
        categories = soup.find("ul", id="listData").find_all("li")
        for cat in categories:
            try:
                title = cat.find("a").get("title")
                url = cat.find("a").get("href")
                yield title, url

            except AttributeError:
                # List contains letters index , should ignore
                continue

        pagination = soup.find("div", "pagination")
        if pagination:
            next_page = pagination.find("a", text=">")
            if next_page:
                for recur_title, recur_url \
                in get_brand_categories(brand_url, recurse=next_page.get("href")):
                    yield recur_title, recur_url
    except AttributeError:
        # Some categories are blank page , do not fall to this trap !
        return


def get_brand_products(category_url, recurse=False):
    """
        Retrieve all products of a brand category.
        Returns: Iterator
    """
    scrap_url = category_url if not recurse else "".join([category_url, recurse])
    brand_products = requests.get(scrap_url)
    soup = bs4.BeautifulSoup(brand_products.text)
    print "processing {}".format(scrap_url)

    try:
        products = soup.find("ul", id="listData").find_all("li")

        for product in products:
            try:
                model = product.find("a").text.encode("utf-8")
                description = product.text.split("-")[1].lstrip().encode("utf-8")
                url = product.find("a").get("href")
                yield model, description, url

            except AttributeError:
                 # Product list contains letters index , should ignore
                continue

        pagination = soup.find("div", "pagination")
        if pagination:
            next_page = pagination.find("a", text=">")
            if next_page:
                for recur_model, recur_description, recur_url \
                in get_brand_products(category_url, recurse=next_page.get("href")):
                    yield recur_model, recur_description, recur_url

    except AttributeError:
        # Some products are blank , do not fall to this trap !
        return


def get_manual_url(product_url):
    product_page = requests.get(product_url)
    soup = bs4.BeautifulSoup(product_page.text)
    try:
        url = soup.find("a", "download-btn").get("href")
    except AttributeError:
        # Some links are 404
        return None

    return url


def download_manual(manual_url, new_filename=False, save_path="manuals"):
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    manual_filename = new_filename or manual_url.split("/")[-1]
    if os.path.exists(os.path.join(save_path, manual_filename)):
        # Do not redownload
        return manual_filename

    r = requests.get(manual_url, stream=True)
    with open(os.path.join(save_path, manual_filename), "wb") as pdf:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                pdf.write(chunk)
                pdf.flush()

    return manual_filename


def get_cached_brands(cache_file):
    cached = set()
    if os.path.exists(cache_file):
        with open(cache_file, "r") as brands_cache:
            cached.update([line.rstrip("\n") for line in brands_cache ])

    return cached

if __name__ == '__main__':
    if len(sys.argv) < 2:
        save_to = "manuals"
    else:
        save_to = sys.argv[1]


    config = json.load(open("../conf/config.json"))
    db_conn = pymysql.connect(**config["mysql"])
    cursor = db_conn.cursor()

    for brand_name, brand_url in get_brands(SCRAP_TARGET + "brands", verbose=True):
        cursor.execute(queries["insert_brand"], brand_name)
        db_conn.commit()
        brand_id = db_conn.insert_id()

        brand_categories = get_brand_categories(SCRAP_TARGET + brand_url)
        for cat_title, cat_url in brand_categories:
            cursor.execute(queries["insert_category"], cat_title)
            db_conn.commit()
            cat_id = db_conn.insert_id()

            products = get_brand_products(cat_url)
            for prod_model, prod_descr, prod_url in products:
                manual_url = get_manual_url(prod_url)
                manual_filename = download_manual(manual_url, save_path=save_to) if manual_url else None

                cursor.execute(queries["insert_product"],
                               [prod_model,
                               prod_descr,
                               cat_id,
                               brand_id,
                               manual_filename])

                db_conn.commit()

    cursor.close()
    db_conn.close()
