<div class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img class="main-logo" src="/img/logo.png" />
                <span class="logo-text">
                    <b>SOS</b>MANUALS
                </span>
            </a>
        </div>   
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <form class="navbar-form navbar-left input-group search" role="search" method="get" action="/search">
                        <div class="input-group">
                            <input type="text" class="form-control search_text" placeholder="Search" name="keyword">
                            <button class="btn btn-default input-group-addon search_button" type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </div>
                    </form> 
                </li>
                <li><a href="/">Home</a></li>
                <li><a href="/brands/a">Brands Index</a></li>
                <li><a href="/categories">Product Categories</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</div>
<!-- <div class="main-wrapper"> -->
