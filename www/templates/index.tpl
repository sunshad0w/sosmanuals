{% include "header.tpl" %}

<div class="wrapper homepage">

    <div class="row">
        <!-- <div class="col-md-2 col-sm-2 hidden-xs left-banner">
            {% include "banners/vertical.tpl" %}
        </div> -->
        <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- {% include "banners/horizontal.tpl" %} -->
            <div class="col-md-12 col-sm-12 col-xs-12 main-container">
                
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Search manuals:</h3>
                    </div>
                    <div class="panel-body">
                        {% include "search/bar.tpl" %}
                    </div>
                </div>
                
                <!-- <div class="col-md-5 col-sm-5 col-xs-12">
                    {% include "banners/horizontal.tpl" %}
                </div> -->
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 main-container">
                <!-- {% include "banners/horizontal.tpl" %} -->
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 main-container">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Featured Categories<a href="/categories">All Categories</a></h3>
                    </div>
                    <div class="panel-body">
                        
                        <a class="featured col-md-3 col-sm-4 col-xs-6" href="categories/7/brands" class="col-md-3 col-sm-4 col-xs-6">
                            <img src="img/cat/laptop.png">
                            <span class="brand-name">Laptops</span>
                        </a>
                        <a class="featured col-md-3 col-sm-4 col-xs-6" href="categories/940/brands" class="col-md-3 col-sm-4 col-xs-6">
                            <img src="img/cat/bike.png">
                            <span class="brand-name">Bicycles</span>
                        </a>
                        <a class="featured col-md-3 col-sm-4 hidden-xs" href="categories/50/brands" class="col-md-3 col-sm-4 col-xs-6">
                            <img src="img/cat/printer.png">
                            <span class="brand-name">Printers</span>
                        </a>
                        <a class="featured col-md-3 hidden-sm hidden-xs" href="categories/42/brands" class="col-md-3 col-sm-hidden col-xs-6">
                            <img src="img/cat/phone.png">
                            <span class="brand-name">Mobile Phones</span>
                        </a>

                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 main-container">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Featured Brands <a href="/brands/a">All Brands</a></h3>
                    </div>
                    <div class="panel-body">
                        <a class="featured col-md-3 col-sm-4 col-xs-6" href="brands/440/categories" class="block">
                            <img src="img/brands/logos/apple.png">
                            <span class="brand-name">Apple</span>
                        </a>
                        <a class="featured col-md-3 col-sm-4 col-xs-6" href="brands/3513/categories" class="block">
                            <img src="img/brands/logos/lg.png">
                            <span class="brand-name">LG Electronics</span>
                        </a>
                        <a class="featured col-md-3 col-sm-4 hidden-xs" href="brands/2617/categories" class="block">
                            <img src="img/brands/logos/google.png">
                            <span class="brand-name">Google</span>
                        </a>
                        <a class="featured col-md-3 hidden-sm hidden-xs" href="brands/5180/categories" class="block">
                            <img src="img/brands/logos/samsung.png">
                            <span class="brand-name">Samsung</span>
                        </a>
                    </div>
                </div>
                <!-- {% include "banners/horizontal.tpl" %} -->
            </div>
        </div>
    </div>
</div>  

{% include "footer.tpl" %}