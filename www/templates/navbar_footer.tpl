<div id="footer">
    <div class="container-fluid">
        <p class="navbar-text navbar-left ">&copy; SosManuals 2014</p>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/">Home</a></li>
            <li><a href="/brands/a">Brands Index</a></li>
            <li><a href="/categories">Product Categories</a></li>
            <li><a href="#terms-of-use" class="popup-trigger" data-role="tos">Terms Of Use</a></li>
            <li><a href="#privacy" class="popup-trigger" data-role="privacy">Privacy</a></li>
            <li><a href="mailto:info@sosmanuals.com">Contact Us</a></li>
        </ul>
    </div>
</div>
{% include "popups.tpl" %}