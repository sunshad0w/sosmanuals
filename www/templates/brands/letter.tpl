{% include "header.tpl" %}

<div class="wrapper list col-md-12 col-sm-12 col-xs-12">
    <!-- {% include "banners/horizontal.tpl" %} -->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Search manuals:</h3>
        </div>
        <!-- <span>Browse other latters</span> -->
        <div class="panel-body">
            {% for letter in alphabet|sort() -%}
               <a href="/brands/{{ letter|lower }}"> {{ letter }} </a>
            {%- endfor %}
        </div>
    </div>

    <!-- {% include "banners/horizontal.tpl" %} -->
    
    {% if brands is defined %}
   <!--  <div class="result-group"> -->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h2 class="panel-title"> {{ letter }} </h2>
        </div>
        <div class="panel-body">
            <ul>
            {% for brand in brands -%}
                <li><a href="/brands/{{ brand.id }}/categories">{{ brand.name }}</a></li>
            {%- endfor %}
            </ul>
        </div>
    </div>
    {% endif %}
</div>


<!-- <div class="right-ad-small col-md-2 col-sm-2 hidden-xs">
    {% include "banners/vertical.tpl" %}
    {% include "banners/vertical.tpl" %}
</div> -->

{% include "footer.tpl" %}
