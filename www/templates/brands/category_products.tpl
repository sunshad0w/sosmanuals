{% include "header.tpl" %}

<div class="wrapper list col-md-12 col-sm-12 col-xs-12">
    <!-- {% include "banners/horizontal.tpl" %} -->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <a href="/brands/{{ brand.id }}/categories">{{ brand.name }}</a>
                <span>/</span>
                <a href="/categories/{{ category.id }}/brands">{{ category.title }}</a>
            </h3>
        </div>
        <div class="panel-body">
            <ul>
            {%- for product in products -%}
                {% if product.url %}
                      <li><a href="{{ product.url }}" target="_blank">{{ product.model }}</a></li>
                {% else %}
                        <li><a href="/viewer/{{ product.id }}">{{ product.model }}</a></li>
                {% endif %}

            {%- endfor %}
            </ul>
            
        </div>
    </div>
    <!-- {% include "banners/horizontal.tpl" %} -->
</div>

<!-- <div class="right-ad-small col-md-2 col-sm-2 hidden-xs">
    {% include "banners/vertical.tpl" %}
    {% include "banners/vertical.tpl" %}
</div> -->

{% include "footer.tpl" %}