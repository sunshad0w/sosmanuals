{% include "header.tpl" %}
<div class="wrapper search col-md-12 col-sm-12 col-xs-12">
    {% if products %} <!-- check if there are any results -->
    <!-- {% include "banners/horizontal.tpl" %} -->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"> Results for &quot;{{ keyword }}&quot; </h3>
            <h5 class="panel-title"> 
                Showing: 
                {% if page < total_pages %}
                    {{ 1 + (page - 1)*10 }} - {{ page * 10 }}
                {% else %}
                    {{ 1 + (page - 1)*10 }} - {{ total_found }}
                {% endif %}
                / 
                {{ total_found }} 
            </h5>
        </div>
        {% for product in products -%}
            <div class="search-result col-md-6 col-sm-6 col-xs-12">
                <div class="search-image col-xs-5">
                    <!-- {{ product }} -->
                    {% if product.url %}
                        <a href="{{ product.url }}" target="_blank">
                    {% else %}
                        <a href="/viewer/{{ product.id }}">
                    {% endif %}
                        <img src="/thumbs/{{ product.filename | replace('.pdf', '.jpeg') }}"
                        onerror="this.src='/img/noimg.jpeg'; this.className='noimg'"
                        alt="{{ product.model }}" />
                    </a>
                </div>
                <div class="search-info col-xs-7">
                    {% if product.url %}
                        <a href="{{ product.url }}" target="_blank">
                    {% else %}
                        <a href="/viewer/{{ product.id }}">
                    {% endif %}
                        <h4> {{ product.model }} </h4>
                    </a>
                    <a class="similar-items" href="/brands/{{ product.brand }}/category/{{ product.category }}">Similar items</a>
                </div>
            </div>
        {%- endfor %}
        <!-- <div class="col-xs-12 col-md-12 col-sm-12">
            {% include "banners/horizontal.tpl" %}
        </div> -->
        <div class="pagination-wrapper">

        <!-- ========== -->
        <!-- PAGINATION -->
        <!-- ========== -->

        <ul class="pagination pagination-sm">
            <li{% if page <= 1  %} class="hidden" {% endif %}>
                <a href="/search?keyword={{ keyword }}&page={{ 1 }}">
                    <span class="glyphicon glyphicon-fast-backward"></span>
                </a>
            </li>
            <li{% if page <= 1  %} class="hidden" {% endif %}>
                <a href="/search?keyword={{ keyword }}&page={{ page - 1 }}">
                    <span class="glyphicon glyphicon-backward"></span>
                </a>
            </li>
            
            {%set start = page - 2 %}
            {%set end = page + 3 %}

            {% if page <=2 %}
                {%set start = 1 %}
                {%set end = 6 %}
            {% endif %}

            {% if page >= total_pages - 2 %}
                {%set start = total_pages - 4 %}
                {%set end = total_pages + 1 %}
            {% endif %}

            {% for n in range(start, end) -%}
                {% if (n >= 1)  %}
                    {% if (n < total_pages + 1)  %}
                        <li {% if n == page %} class="active" {% endif %}><a href="/search?keyword={{ keyword }}&page={{ n }}">{{ n }}</a></li>
                    {% endif %}
                {% endif %}
            {%- endfor %}

            <li {% if page >= total_pages  %} class="hidden" {% endif %}>
                <a href="/search?keyword={{ keyword }}&page={{ page + 1 }}">
                    <span class="glyphicon glyphicon-forward"></span>
                </a>
            </li>
            <li {% if page >= total_pages  %} class="hidden" {% endif %}>
                <a href="/search?keyword={{ keyword }}&page={{ total_pages }}">
                    <span class="glyphicon glyphicon-fast-forward"></span>
                </a>
            </li>
        </ul><!-- PAGINATION END -->
        </div>
    </div>
    {% else %}

    <div class="alert alert-dismissable alert-warning">
        <h4>No results found!</h4>
        <p>Please check your spelling and try searching again.</p>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Search manuals:</h3>
        </div>
        <div class="panel-body">
            {% include "search/bar.tpl" %}
        </div>
    </div>

    {% endif %}
</div>

<!-- <div class="right-ad-small col-md-2 col-sm-2 hidden-xs">&nbsp;
    {% include "banners/vertical.tpl" %}
    {% include "banners/vertical.tpl" %}
</div> -->

{% include "footer.tpl" %}