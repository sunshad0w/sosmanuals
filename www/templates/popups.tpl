<div class="popup" id="tos">
    <div class="popup-body">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Term Of Use: <a class="btnClose">Close</a></h3>
            </div>
            <div class="panel-body">
                <div class="text-holder">
                    <h5>1. Terms</h5>

                    <p>By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trade mark law.</p>

                    <h5>2. Use License</h5>

                    <p>Permission is granted to temporarily download one copy of the materials (information or software) on Goldbar Ventures's web site for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:
                    modify or copy the materials;
                    use the materials for any commercial purpose, or for any public display (commercial or non-commercial);
                    attempt to decompile or reverse engineer any software contained on Goldbar Ventures's web site;
                    remove any copyright or other proprietary notations from the materials; or
                    transfer the materials to another person or "mirror" the materials on any other server.
                    This license shall automatically terminate if you violate any of these restrictions and may be terminated by Goldbar Ventures at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.</p>

                    <h5>3. Disclaimer</h5>

                    <p>The materials on Goldbar Ventures's web site are provided "as is". Goldbar Ventures makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Goldbar Ventures does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.</p>

                    <h5>4. Limitations</h5>

                    <p>In no event shall Goldbar Ventures or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on Goldbar Ventures's Internet site, even if Goldbar Ventures or a Goldbar Ventures authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.</p>

                    <h5>5. Revisions and Errata</h5>

                    <p>The materials appearing on Goldbar Ventures's web site could include technical, typographical, or photographic errors. Goldbar Ventures does not warrant that any of the materials on its web site are accurate, complete, or current. Goldbar Ventures may make changes to the materials contained on its web site at any time without notice. Goldbar Ventures does not, however, make any commitment to update the materials.</p>

                    <h5>6. Links</h5>

                    <p>Goldbar Ventures has not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Goldbar Ventures of the site. Use of any such linked web site is at the user's own risk.</p>

                    <h5>7. Site Terms of Use Modifications</h5>

                    <p>Goldbar Ventures may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use.</p>

                    <h5>8. Governing Law</h5>

                    <p>Any claim relating to Goldbar Ventures's web site shall be governed by the laws of the State of Toronto without regard to its conflict of law provisions.</p>

                    <p>General Terms and Conditions applicable to Use of a Web Site.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="popup" id="privacy">
    <div class="popup-body">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Privacy: <a class="btnClose">Close</a></h3>
            </div>
            <div class="panel-body">
                <div class="text-holder">
                    <h5><b>Our Policy</b></h5>

                    <p>This Privacy Policy describes SOS Manuals's policies and procedures on the collection, use and disclosure of your information. SOS Manuals receives your information through our various web sites, SMS, APIs, applications, services and third-parties ("Services"). For example, you send us information when you use SOS Manuals from our web site, post or receive Tweets via SMS, add a SOS Manuals widget to your website, or access SOS Manuals from an application such as SOS Manuals for Mac, SOS Manuals for Android or TweetDeck. When using any of our Services you consent to the collection, transfer, manipulation, storage, disclosure and other uses of your information as described in this Privacy Policy. Irrespective of which country that you reside in or create information from, your information may be used by SOS Manuals in the United States or any other country where SOS Manuals operates. If you have any questions or comments about this Privacy Policy, please contact us at info@SOS Manuals.com.</p>

                    <p>SOS Manuals is integrated with Google Maps, and therefore by using SOS Manuals you agree to be bound by Google's Terms of Use. Please read Google Maps Privacy Policy</p>

                    <h5><b>Information Collection and Use</b></h5>

                    <p><b>Information Collected Upon Registration:</b> When you create or reconfigure a SOS Manuals account, you provide some personal information, such as your name, username, password, and email address. Some of this information, for example, your name and username, is listed publicly on our Services, including on your profile page and in search results. Some Services, such as search, public user profiles and viewing lists, do not require registration.</p>

                    <p><b>Additional Information:</b> You may provide us with additional information to make public, such as a short biography, your location, or a picture. You may customize your account with information such as a cell phone number for the delivery of SMS messages or your address book so that we can help you find SOS Manuals users you know. We may use your contact information to send you information about our Services or to market to you. You may unsubscribe from these messages by following the instructions contained within the messages or the instructions on our web site. If you email us, we may keep your message, email address and contact information to respond to your request. Providing the additional information described in this section is entirely optional.</p>

                    <p><b>Public Information:</b> Our Services are primarily designed to help you share information with the world. Most of the information you provide to us is information you are asking us to make public. This includes not only the messages you Tweet and the metadata provided with Tweets, such as when you Tweeted, but also the lists you create, the people you follow, the Tweets you mark as favorites or Retweet and many other bits of information. Our default is almost always to make the information you provide public but we generally give you settings (https://www.SOS Manuals.com/account/settings) to make the information more private if you want. Your public information is broadly and instantly disseminated. For example, your public Tweets are searchable by many search engines and are immediately delivered via SMS and our APIs (http://www.sosmanuals.com/pages/api_faq) to a wide range of users and services. You should be careful about all information that will be made public by SOS Manuals, not just your Tweets.</p>

                    <h5><b>Information Sharing and Disclosure</b></h5>

                    <p><b>Your Consent:</b> When you create or reconfigure a SOS Manuals account, you provide some personal information, such as your name, username, password, and email address. Some of this information, for example, your name and username, is listed publicly on our Services, including on your profile page and in search results. Some Services, such as search, public user profiles and viewing lists, do not require registration.</p>
                </div>
            </div>
        </div>
    </div>
</div>