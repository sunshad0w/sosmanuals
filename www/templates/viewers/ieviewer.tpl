<!DOCTYPE html>
<html>
 <head>
  <head profile="http://www.w3.org/2005/10/profile">
  <link rel="shortcut icon" type="image/x-icon" href="/img/favicon/favicon.ico" />
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SOSManuals</title>
  <!-- FAVICON -->
  <!-- <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-touch-icon-76x76.png">
  <link rel="icon" type="image/png" href="/img/favicon/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="/img/favicon/favicon-16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="/img/favicon/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/x-icon" href="/img/favicon/favicon.ico"/> -->
  <!-- <meta name="msapplication-TileColor" content="#da532c"> -->

  <link rel="stylesheet" href="/css/main.min.css">
  <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
  <script type="text/javascript" src="/js/viewer/pdfobject.js"></script>
  <!-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> -->
  <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-57987874-1', 'auto');
      ga('send', 'pageview');
  </script>
 </head>
 <body>
  {% include "navbar.tpl" %}
  <div class="main-wrapper ieviewer">
    <div class="pure-g">
      <div class="pure-u-1-5"></div>
          <div id="viewer" class="pure-u-3-5">
            <h2>It appears you don't have Adobe Reader or PDF support in this web browser. </h2>
          </div>
      <div class="pure-u-1-5"></div>    
    </div>
  </div>

  {% include "footer.tpl" %}
  
  <script type="text/javascript">
    window.onload = function (){
        var manual = new PDFObject({ 
            url: "/manuals/{{ manual_filename }}",
            height: "100%"
        });
        if (manual) {
            manual.embed("viewer");
        }
    };
  </script>
  <script data-main="/js/main" src="/js/lib/requirejs/require.js"></script>
 </body>
</html>
