{% include "header.tpl" %}

<div class="wrapper list">
    <h1> {{ category.title }} </h1>
    {% for letter in products|sort() -%}
        <h2>{{ letter }}</h2>
        <ul>
        {% for product in products[letter] -%}
            {% if product.url %}
            <li><a href="{{ product.url }}" target="_blank">{{ product.model }}</a></li>
            {% else %}
            <li><a href="/viewer/{{ product.id }}">{{ product.model }}</a></li>
            {% endif %}
        {% endfor %}
        </ul>
    {%- endfor %}
</div>

{% include "footer.tpl" %}