{% include "header.tpl" %}

<div class="wrapper list col-md-12 col-sm-12 col-xs-12">
    <!-- {% include "banners/horizontal.tpl" %} -->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Categories</h3>
        </div>
        <div class="panel-body">
            {% for letter in categories.keys()|sort() -%}
               <!-- <h2 id="letter-{{ letter }}"> {{ letter }} <a href="#">back to top &uarr;</a></h2> -->
               <a href="#{{ letter }}"> {{ letter }} </a>
            {%- endfor %}
        </div>
    </div>
    <!-- {% include "banners/horizontal.tpl" %} -->
    {% for letter in categories|sort() -%}
        <div class="panel panel-primary">
          <div class="panel-heading">
              <h3 class="panel-title">
                  {{ letter }} 
                  <a href="#">back to top &uarr;</a>
                  <span class="target" id="{{ letter }}"></span>
              </h3>
          </div>
          <div class="panel-body">
              <ul>
                {% for category in categories[letter] -%}
                  <li title="{{ category.title }}"><a href="/categories/{{category.id}}/brands">{{ category.title }}</a></li>
                {% endfor %}
              </ul>
          </div>
        </div>
    {%- endfor %}
</div>
<!-- <div class="right-ad-small col-md-2 col-sm-2 hidden-xs">
    {% include "banners/vertical.tpl" %}
    {% include "banners/vertical.tpl" %}
</div> -->

{% include "footer.tpl" %}