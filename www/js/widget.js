$(function(){
    dom = {
        brands:         $("#brand"),
        categories:     $("#category"),
        models:         $("#model"),
        submitBtn:      $("button[type='submit']")
    };

    window.onbeforeunload = onBeforeUnload;
    dom.brands.on("change", onBrandChanged);
    dom.categories.on("change", onCategoryChanged);
    dom.models.on("change", onModelChanged);

    // ===== Event Handlers

    function onBeforeUnload(){
        $("form")[0].reset();
    }

    function onBrandChanged(){
        dom.submitBtn.attr("disabled", "disabled");
        $.get("/api/brand/" + this.value + "/categories").then(populateCategories);    
    }

    function onCategoryChanged(){
        dom.submitBtn.attr("disabled", "disabled");
        $.get("/api/brand/"+ dom.brands.find(":selected").val() + "/category/" + this.value + "/products").then(populateModels);
    }

    function onModelChanged(){
        dom.submitBtn.removeAttr("disabled");
        setManualUrl(this.value);
    }

    // ===== API Callbacks

    function populateCategories(category_list){
        dom.models.empty();
        dom.categories.empty().append(createOption(0, "Select Category...", 0));
        $.each(category_list, function(){
            dom.categories.append(createOption(this.id, this.title, 1));
        });
    }

    function populateModels(models_list){
        dom.models.empty().append(createOption(0, "Select Model...", 0));
        $.each(models_list, function(){
            dom.models.append(createOption(this.id, this.model, 1));
        });
    }

    function setManualUrl(product_id){
        $("form").attr("action", "/viewer/" + product_id);
    }

    // ===== Helpers

    function createOption(id, value, state) {
        var opt = $("<option />").val(id).text(value);
        if (state < 1) {
            opt.attr("disabled", "disabled");
            opt.attr("selected", "selected");
        }
        return opt;
    }
});
