require.config({
  paths: {
    'jquery': "lib/jquery/dist/jquery.min",
    'bootstrap': 'lib/bootstrap/dist/js/bootstrap.min',
    //'collapse': 'lib/bootstrap/js/collapse' not needed, or so it would seam...
  },
  shim : { // If the library does not support AMD
        'bootstrap':{deps: ['jquery']}
    }
});

define(['jquery', 'bootstrap'], function($, bootstrap){
  $(".popup-trigger").on("click", showPopup);
  $(".btnClose").on("click", closePopups);
  $(".popup").on("click", closePopups);
  $(".popup-body").on("click", function(e){
    e.stopPropagation();
  });

  function showPopup(){
    $("#" + $(this).attr("data-role")).show();
  }

  function closePopups(){
    $(".popup").hide();
  }

  if(!(window.ActiveXObject) && "ActiveXObject" in window){ //IE11 check
    $("body").addClass("ie11");   
  }
});