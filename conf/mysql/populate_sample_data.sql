use manuela;
INSERT INTO brands(id,name) VALUES(1,"Sony");
INSERT INTO brands(id,name) VALUES(2,"Asus");
INSERT INTO brands(id,name) VALUES(3,"Apple");
INSERT INTO categories(id,title) VALUES(1,"Robots");
INSERT INTO categories(id,title) VALUES(2,"Laptops");
INSERT INTO categories(id,title) VALUES(3,"Phones");
INSERT INTO products(id,model,description,filename,category,brand) VALUES(1,"iRobot X3", "KickAss robot", "a.pdf", 1, 3);
INSERT INTO products(id,model,description,filename,category,brand) VALUES(2,"iPhone 5", "Super phone", "b.pdf", 3, 3);
INSERT INTO products(id,model,description,filename,category,brand) VALUES(3,"iPhone 4", "old phone", "c.pdf", 3, 3);
INSERT INTO products(id,model,description,filename,category,brand) VALUES(4,"FlatBook i5", "super laptop", "d.pdf", 2, 2);
INSERT INTO products(id,model,description,filename,category,brand) VALUES(5,"Vaio", "Kickass Laptop", "e.pdf", 2, 1);
