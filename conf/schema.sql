CREATE DATABASE IF NOT EXISTS manuela;
use manuela;

CREATE TABLE IF NOT EXISTS brands(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS categories(
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(128) NOT NULL,
    UNIQUE KEY(title),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS products(
    id INT NOT NULL AUTO_INCREMENT,
    model VARCHAR(256) NOT NULL,
    description VARCHAR(1024),
    filename VARCHAR(128),
    category INT NOT NULL,
    brand INT NOT NULL,
    PRIMARY KEY (id)
);

# Added 23.10.14

ALTER TABLE products ADD brand_tagged INT;
#DELETE brands FROM brands LEFT JOIN products ON brands.id = products.brand_tagged WHERE products.brand_tagged IS NULL;