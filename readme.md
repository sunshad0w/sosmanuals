Manuela
=======

## Tech Stack:
* [nginx](http://nginx.org/)
* [uWSGI](http://projects.unbit.it/uwsgi/)
* [Python Bottle](http://bottlepy.org/docs/dev/index.html) _( Python 2.7.x )_


## Deployment:

We are going to setup a working stack from a bottom up , starting with __python__ app and going up to __nginx__ web server.  

### Steps :
1. Clone this repo to `/srv` .
2. Database
    * create new mysql user and update `conf/config.json` with relevant data
    * populate database from dump file .
3. Install __pip__ , setup __virtualenv__ and required modules 
    * `apt-get install python-pip`
    * `pip install virtualenv`
    * Load virtualenv in `env` directory
    * `pip install -r requirements.txt`
4. Test python application
    * run `$ pyvows integration.py` from tests directory
5. Install and configure uWSGI
    * `apt-get install uwsgi uwsgi-plugin-python`
    * create a symlink to `conf/wsgi/manuela.ini` in `/etc/uwsgi/apps-enabled`
    *  start uWSGI service
7. Configure nginx
    * create a symlink to `conf/nginx/manuela.conf` in `/etc/nginx/sites-enabled`
    * reload nginx
8. Install Clientside dependencies
    * `npm install` from root directory .
    * `bower install` from root directory .
    * `grunt build` from root directory .
