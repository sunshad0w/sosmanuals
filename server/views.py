import os

import bottle
from bottle import jinja2_template as template
from bottle import request, abort

import models
import utils

bottle.TEMPLATE_PATH.insert(0, "../www/templates")


def index():
    return template("index")


def brands(letter=None):
    return template("brands/letter", models.brands(letter))


def brand_categories(id):
    return template("brands/categories", models.brand_categories(id))


def brand_category_products(bid, cid):
    return template("brands/category_products", models.brand_category_products(bid, cid))


def categories():
    return template("categories/all", models.categories())


def category_brands(id):
    return template("categories/brands", models.category_brands(id))


def search():
    results = models.search(
        request.GET.keyword, 
        request.GET.page or 1,
        request.GET.per_page)

    if "error" in results.keys():
        # Bad request
        abort(400, results["error"])

    return template("search/results", results)


def viewer(id):
    product = models.product(id)
    user_agent = bottle.request.headers["User-Agent"]

    if "MSIE 8.0" in user_agent:
        # IE8 uses pdfobject.js to display content
        return template("viewers/ieviewer", manual_filename=product.get("filename"))
    else:
        # Everything else uses PDF.js
        return template("viewers/viewer", manual_filename=product.get("filename"))
