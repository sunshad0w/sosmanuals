import json

from bottle import request, response, abort

import models
import utils


def get_brands(letter=None):
    response.content_type = 'application/json'
    return json.dumps(models.brands(letter))


def get_brand(id):
    response.content_type = 'application/json'
    return json.dumps(models.brand(id))


def get_category(id):
    response.content_type = 'application/json'
    return json.dumps(models.category(id))


def get_categories():
    response.content_type = 'application/json'
    return json.dumps(models.categories())


def get_brand_products(id):
    response.content_type = 'application/json'
    return json.dumps(models.brand_products(id))


def get_category_brands(id):
    response.content_type = 'application/json'
    return json.dumps(models.category_brands(id))


def get_brand_categories(id):
    response.content_type = 'application/json'
    return json.dumps(models.brand_categories(id))


def get_brand_category_products(bid, cid):
    response.content_type = 'application/json'
    return json.dumps(models.brand_category_products(bid, cid))


def get_product(id):
    response.content_type = 'application/json'
    return json.dumps(models.product(id))


def get_search():
    results = models.search(
        request.GET.keyword, 
        request.GET.page or 1,
        request.GET.per_page)

    if "error" in results.keys():
        # Bad request
        abort(400, results["error"])

    response.content_type = 'application/json'
    return json.dumps(results)
