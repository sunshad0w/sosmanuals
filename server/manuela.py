import os
import sys
from bottle import Bottle
from gevent import monkey
monkey.patch_all

import api
import views

reload(sys)
sys.setdefaultencoding('utf-8')
app = application = Bottle()

# VIEWS
app.route("/", "GET", views.index)
app.route("/brands", "GET", views.brands)
app.route("/brands/<letter:re:([a-z@])>", "GET", views.brands)
app.route("/brands/<id:int>/categories", "GET", views.brand_categories)
app.route("/brands/<bid:int>/category/<cid:int>", "GET", views.brand_category_products)
app.route("/categories", "GET", views.categories)
app.route("/categories/<id:int>/brands", "GET", views.category_brands)
app.route("/viewer/<id:int>", "GET", views.viewer)
app.route("/search", "GET", views.search)

# REST
app.route("/api/brands", "GET", api.get_brands)
app.route("/api/brands/<letter:re:([a-z@])>", "GET", api.get_brands)
app.route("/api/brands/<id:int>", "GET", api.get_brand)
app.route("/api/brands/<id:int>/categories", "GET", api.get_brand_categories)
app.route("/api/brands/<bid:int>/category/<cid:int>", "GET", api.get_brand_category_products)
app.route("/api/categories", "GET", api.get_categories)
app.route("/api/categories/<id:int>", "GET", api.get_category)
app.route("/api/categories/<id:int>/brands", "GET", api.get_category_brands)
app.route("/api/product/<id:int>", "GET", api.get_product)
app.route("/api/search", "GET", api.get_search)


if __name__ == '__main__':
    # Dev server
    app.run(host="0.0.0.0", port=8083, debug=True, reloader=True)
