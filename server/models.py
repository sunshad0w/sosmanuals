import json
import pymysql

import utils

ITEMS_PER_SEARCH_RESULT = 10
config = json.load(open("../conf/config.json"))
queries = dict(
    brands="SELECT name, id FROM brands",
    brand="SELECT name, id FROM brands WHERE id=%s",
    categories="SELECT title, id FROM categories",
    category="SELECT title, id FROM categories WHERE id=%s",
    brand_products="SELECT model, filename, id FROM products WHERE brand=%s",
    category_brands="SELECT DISTINCT brands.id, brands.name FROM brands INNER JOIN products on products.brand_tagged = brands.id WHERE products.category=%s ORDER BY brands.name",
    brand_categories="SELECT DISTINCT a.title, a.id FROM categories a, products b WHERE a.id = b.category AND b.brand_tagged=%s",
    brand_category_products="SELECT model, filename, id, url FROM products WHERE brand_tagged=%s AND category=%s",
    product="SELECT model, filename, id, url FROM products WHERE id=%s",
    search="SELECT model, filename, id, url, category, brand FROM products WHERE MATCH(model) AGAINST(%s IN BOOLEAN MODE)")


def db_connect():
    db_conn = pymysql.connect(**config["mysql"])
    cursor = db_conn.cursor(pymysql.cursors.DictCursor)
    return db_conn, cursor


def brand(brand_id):
    db_conn, cursor = db_connect()
    cursor.execute(queries.get("brand"), brand_id)
    brand = cursor.fetchone()
    cursor.close()
    db_conn.close()
    return brand


def brands(letter=None):
    db_conn, cursor = db_connect()
    cursor.execute(queries.get("brands"))
    brands = cursor.fetchall()
    cursor.close()
    db_conn.close()

    brands_sorted = utils.sort_alphabeticaly(brands, "name")
    result = {"alphabet": brands_sorted.keys()}
    if letter is not None:
        lu = letter.upper()
        result.update({
            "brands": brands_sorted.get(lu),
            "letter": lu})

    return result


def category(category_id):
    db_conn, cursor = db_connect()
    cursor.execute(queries.get("category"), category_id)
    category = cursor.fetchone()
    cursor.close()
    db_conn.close()
    return category


def categories():
    db_conn, cursor = db_connect()
    cursor.execute(queries.get("categories"))
    categories = cursor.fetchall()
    cursor.close()
    db_conn.close()
    return {"categories" :utils.sort_alphabeticaly(categories, "title")}


def category_brands(category_id):
    db_conn, cursor = db_connect()
    cursor.execute(queries.get("category_brands"), category_id)
    brands = cursor.fetchall()
    cursor.close()
    db_conn.close()
    return {
        "category": category(category_id),
        "brands": brands
    }


def brand_categories(brand_id):
    db_conn, cursor = db_connect()
    cursor.execute(queries.get("brand_categories"), brand_id)
    categories = cursor.fetchall()
    cursor.close()
    db_conn.close()
    return {
        "brand": brand(brand_id),
        "categories": categories
    }


def brand_category_products(brand_id, category_id):
    db_conn, cursor = db_connect()
    cursor.execute(queries.get("brand_category_products"), (brand_id, category_id))
    products = cursor.fetchall()
    cursor.close()
    db_conn.close()
    return {
        "brand": brand(brand_id),
        "category": category(category_id),
        "products": products
    }


def product(product_id):
    db_conn, cursor = db_connect()
    cursor.execute(queries.get("product"), product_id)
    product = cursor.fetchone()
    cursor.close()
    db_conn.close()
    return product


def search(keyword, page, per_page):
    if not keyword:
        return {"error": "No search keyword"}

    try:
        page = int(page)
        per_page = int(per_page or ITEMS_PER_SEARCH_RESULT)
    except ValueError:
        return {"error": "Invalid page index"}

    db_conn, cursor = db_connect()
    cursor.execute(queries.get("search"), keyword)
    products = cursor.fetchall()
    cursor.close()
    db_conn.close()

    if len(products) is 0:
        return {}

    try:
        products_chunked = list(utils.chunks(products, per_page))
        products_chunk = products_chunked[page - 1]
    except IndexError:
        return {"error": "Invalid page index"}

    return {
        "products": products_chunk,
        "total_pages": len(products_chunked),
        "total_found": len(products),
        "keyword": keyword,
        "page": page
    }
