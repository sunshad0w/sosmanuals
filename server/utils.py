import string
import collections


def sort_alphabeticaly(items, sort_key):
    result = collections.defaultdict(list)
    for item in items:
        letter = item.get(sort_key)[0].upper()
        if letter in string.ascii_uppercase:
            result[letter].append(item)
        else:
            result["@"].append(item)

    return result


def chunks(l, n):
    # Yield n-sized chunks from l.
    for i in xrange(0, len(l), n):
        yield l[i:i+n]
