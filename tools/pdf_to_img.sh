#!/bin/bash

# This script extracts first page preview for pdf files
# Depends on : GhostScript

if [ $# -ne 2 ]; then
    echo "Usage: $(basename $0) /path/to/pdf/files /path/to/output/dir"
    exit
fi

dir_in=$1
dir_out=$2


for filename in $dir_in/*.pdf; do
    echo "Processing $filename"
    basename="$(basename "$filename" .pdf)"
    if [ -f $dir_out/${basename%.*}.jpeg ]; then
        echo "Exists, skipping ..."
        continue
    fi
    
    gs -sDEVICE=jpeg -dFirstPage=1 -dLastPage=1 -dDOINTERPOLATE -dUseTrimBox -q \
    -dBATCH -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r400 -g400x600 -dPDFFitPage=true  \
    -o $dir_out/${basename%.*}.jpeg $filename
done
