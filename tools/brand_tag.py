import os
import sys
import json
from pprint import pprint

import pymysql


def max_weight(prev, next):
    return prev if prev["weight"] > next["weight"] else next


def to_weight(record):
    """
                    freq   / index
        Weight =    ----  /  -----
                     WC  /     CC

        WC = Words in content
        CC = Chars in content
    """
    return {
        "brand": record["name"],
        "id": record["id"],
        "weight": float(record["freq"]) / len(content.split(" ")) * len(content) / record["index"]
    }


if len(sys.argv) != 2:
    print "Usage: categorize.py /path/to/content"
    exit(1)


# Extracted text data directory
content_dir = sys.argv[1]
config = json.load(open("../conf/config.json"))
if not os.path.exists(content_dir):
    print("Path does not exist. Please supply valid path")
    exit(1)

log_file = open("categorize.log", "w")

db_conn = pymysql.connect(**config["mysql"])
cursor = db_conn.cursor(pymysql.cursors.DictCursor)

cursor.execute("SELECT * FROM brands")
brands = cursor.fetchall()

cursor.execute("SELECT * FROM categories")
categories = cursor.fetchall()

cursor.execute("SELECT * FROM products")  # LIMIT 300
products = cursor.fetchall()


result = []
for i, product in enumerate(products):
    print "{} of {} complete              \r".format(i, len(products)),
    brands_found = []

    try:
        content_path = os.path.join(content_dir, product["filename"].replace(".pdf", ".txt"))
        content_file = open(content_path)
    except:
        log_file.write("Error processing: {}, Description: {}\n".format(product, sys.exc_info()[0]))
        continue

    content = content_file.read().replace("\n", " ").lower()
    for brand in brands:
        brand_name = " {} ".format(brand["name"].lower())
        if brand_name in content:
            brands_found.append({
                "id":   brand["id"],
                "name": brand["name"],
                "index": content.index(brand_name) + 1,
                "freq": content.count(brand_name)
            })

    content_file.close()
    if not brands_found:
        continue

    result.append({
        "id": product["id"],
        "detected_brand": reduce(max_weight, map(to_weight, brands_found))
    })


for res in result:
    cursor.execute("UPDATE products SET brand_tagged=%s WHERE id=%s", (res.get("detected_brand")["id"], res.get("id")))

db_conn.commit()
cursor.close()
db_conn.close()
