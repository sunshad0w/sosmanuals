import json
import pymysql

config = json.load(open("../conf/config.json"))

db_conn = pymysql.connect(**config["mysql"])
cursor = db_conn.cursor(pymysql.cursors.DictCursor)

cursor.execute("SELECT id FROM brands")
brands = cursor.fetchall();

for brand in brands:
    cursor.execute("SELECT id FROM products WHERE brand_tagged = %s", brand.get("id"))
    if len(cursor.fetchall()) is 0:
        print "Deleting {}".format(brand.get("id"))
        cursor.execute("DELETE FROM brands WHERE id =%s", brand.get("id"))
        db_conn.commit()


cursor.close()
db_conn.close()
