#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $(basename $0) /path/to/pdf/files"
    exit
fi

for F in $1/*.pdf; do
    md5_name="$(md5sum "$F" | cut -d' ' -f1).${F##*.}";
    mv "$F" $1/$md5_name
done
