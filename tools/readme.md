Tools
=====

This directory contains various utilities that help categorise/tag scrapped pdf documents.


### pdf_to_text.sh

Converts first 2 pages of every pdf file to text data , so `brand_tag.py` can work with the data.  
__Usage:__ ` ./pdf_to_text.sh /path/to/pdf/files /path/to/output/dir`


### pdf_to_img.sh

Converts first page of every pdf file to PNG image , wich are later used to preview thumbnails .  
__Usage:__ ` ./pdf_to_img.sh /path/to/pdf/files /path/to/output/dir`


### brand_tag.py

Tries to find brand names in text extracted from pdf files , if found it assigns a brand `id`
to `manuela.products.brand_tagged` column .  
List of brand names is found in `manuela.brands` .