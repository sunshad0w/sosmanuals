#!/bin/bash

# This script extracts first 2 pages from pdf data to a text file
# Depends on : poppler-utils

if [ $# -ne 2 ]; then
    echo "Usage: $(basename $0) /path/to/pdf/files /path/to/output/dir"
    exit
fi

dir_in=$1
dir_out=$2

for filename in $dir_in/*.pdf; do
    echo "Processing $filename"
    basename="$(basename "$filename" .pdf)"
    pdftotext -f 1 -l 2 $dir_in/$pdf_file $dir_out/${basename%.*}.txt
done